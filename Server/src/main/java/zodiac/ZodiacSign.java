package zodiac;

public class ZodiacSign {
    public String zodiacSign;
    public String startDate;
    public String endDate;

    public ZodiacSign(String zodiacSign, String startDate, String endDate) {
        this.zodiacSign = zodiacSign;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
