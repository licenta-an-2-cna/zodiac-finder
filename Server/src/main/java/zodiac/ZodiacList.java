package zodiac;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ZodiacList {
    private ArrayList<ZodiacSign> zodiacSignArrayList = new ArrayList<ZodiacSign>();

    public ZodiacList() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("src/main/resources/zodiacSignIntervals.txt"));
            String line = reader.readLine();
            while (line != null) {
                String[] sign = line.split(" ");

                ZodiacSign zodiacSign = new ZodiacSign(sign[0], sign[1], sign[2]);
                zodiacSignArrayList.add(zodiacSign);

                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String ReturnSign(String date) {
        try {
            String[] dateSplit = date.split("/");
            int month = Integer.parseInt(dateSplit[0]);
            int day = Integer.parseInt(dateSplit[1]);

            for (ZodiacSign zodiacSign : zodiacSignArrayList) {
                String[] startDate = zodiacSign.startDate.split("/");
                String[] endDate = zodiacSign.endDate.split("/");

                if ((month == Integer.parseInt(startDate[0]) && day >= Integer.parseInt(startDate[1])) || (month == Integer.parseInt(endDate[0]) && day <= Integer.parseInt(endDate[1]))) {
                    return zodiacSign.zodiacSign;
                }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }
}
