package services;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.ZodiacSign;
import proto.ZodiacSignServiceGrpc;
import zodiac.ZodiacList;

public class ZodiacSignService extends ZodiacSignServiceGrpc.ZodiacSignServiceImplBase {
    @Override
    public void getZodiacSign(ZodiacSign.Date request, StreamObserver<ZodiacSign.ZodiacSignResponse> responseObserver) {
        ZodiacSign.ZodiacSignResponse.Builder response = ZodiacSign.ZodiacSignResponse.newBuilder();

        System.out.println("Client Date: " + request.getDate());

        ZodiacList zodiacList = new ZodiacList();
        String zodiacSignOfClient = zodiacList.ReturnSign(request.getDate());
        if(zodiacSignOfClient.equals("")) {
            Status status = Status.INVALID_ARGUMENT.withDescription("Date format invalid - MM/DD/YYYY");
            responseObserver.onError(status.asRuntimeException());
        }

        response.setZodiacSign(zodiacSignOfClient);
        responseObserver.onNext(response.build());
        System.out.println(response);
        responseObserver.onCompleted();
    }
}
