package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: ZodiacSign.proto")
public final class ZodiacSignServiceGrpc {

  private ZodiacSignServiceGrpc() {}

  public static final String SERVICE_NAME = "ZodiacSignService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.ZodiacSign.Date,
      proto.ZodiacSign.ZodiacSignResponse> getGetZodiacSignMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getZodiacSign",
      requestType = proto.ZodiacSign.Date.class,
      responseType = proto.ZodiacSign.ZodiacSignResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.ZodiacSign.Date,
      proto.ZodiacSign.ZodiacSignResponse> getGetZodiacSignMethod() {
    io.grpc.MethodDescriptor<proto.ZodiacSign.Date, proto.ZodiacSign.ZodiacSignResponse> getGetZodiacSignMethod;
    if ((getGetZodiacSignMethod = ZodiacSignServiceGrpc.getGetZodiacSignMethod) == null) {
      synchronized (ZodiacSignServiceGrpc.class) {
        if ((getGetZodiacSignMethod = ZodiacSignServiceGrpc.getGetZodiacSignMethod) == null) {
          ZodiacSignServiceGrpc.getGetZodiacSignMethod = getGetZodiacSignMethod = 
              io.grpc.MethodDescriptor.<proto.ZodiacSign.Date, proto.ZodiacSign.ZodiacSignResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "ZodiacSignService", "getZodiacSign"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.ZodiacSign.Date.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.ZodiacSign.ZodiacSignResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ZodiacSignServiceMethodDescriptorSupplier("getZodiacSign"))
                  .build();
          }
        }
     }
     return getGetZodiacSignMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ZodiacSignServiceStub newStub(io.grpc.Channel channel) {
    return new ZodiacSignServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ZodiacSignServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ZodiacSignServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ZodiacSignServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ZodiacSignServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ZodiacSignServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getZodiacSign(proto.ZodiacSign.Date request,
        io.grpc.stub.StreamObserver<proto.ZodiacSign.ZodiacSignResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetZodiacSignMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetZodiacSignMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.ZodiacSign.Date,
                proto.ZodiacSign.ZodiacSignResponse>(
                  this, METHODID_GET_ZODIAC_SIGN)))
          .build();
    }
  }

  /**
   */
  public static final class ZodiacSignServiceStub extends io.grpc.stub.AbstractStub<ZodiacSignServiceStub> {
    private ZodiacSignServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignServiceStub(channel, callOptions);
    }

    /**
     */
    public void getZodiacSign(proto.ZodiacSign.Date request,
        io.grpc.stub.StreamObserver<proto.ZodiacSign.ZodiacSignResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetZodiacSignMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ZodiacSignServiceBlockingStub extends io.grpc.stub.AbstractStub<ZodiacSignServiceBlockingStub> {
    private ZodiacSignServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public proto.ZodiacSign.ZodiacSignResponse getZodiacSign(proto.ZodiacSign.Date request) {
      return blockingUnaryCall(
          getChannel(), getGetZodiacSignMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ZodiacSignServiceFutureStub extends io.grpc.stub.AbstractStub<ZodiacSignServiceFutureStub> {
    private ZodiacSignServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.ZodiacSign.ZodiacSignResponse> getZodiacSign(
        proto.ZodiacSign.Date request) {
      return futureUnaryCall(
          getChannel().newCall(getGetZodiacSignMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ZODIAC_SIGN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ZodiacSignServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ZodiacSignServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ZODIAC_SIGN:
          serviceImpl.getZodiacSign((proto.ZodiacSign.Date) request,
              (io.grpc.stub.StreamObserver<proto.ZodiacSign.ZodiacSignResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ZodiacSignServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ZodiacSignServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.ZodiacSign.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ZodiacSignService");
    }
  }

  private static final class ZodiacSignServiceFileDescriptorSupplier
      extends ZodiacSignServiceBaseDescriptorSupplier {
    ZodiacSignServiceFileDescriptorSupplier() {}
  }

  private static final class ZodiacSignServiceMethodDescriptorSupplier
      extends ZodiacSignServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ZodiacSignServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ZodiacSignServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ZodiacSignServiceFileDescriptorSupplier())
              .addMethod(getGetZodiacSignMethod())
              .build();
        }
      }
    }
    return result;
  }
}
