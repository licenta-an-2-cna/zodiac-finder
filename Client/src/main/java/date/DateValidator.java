package date;

public interface DateValidator {
    boolean isValid(String dateStr);
}
