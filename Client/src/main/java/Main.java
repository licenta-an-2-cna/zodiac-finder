import date.DateValidator;
import date.DateValidatorUsingDateFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.ZodiacSign;
import proto.ZodiacSignServiceGrpc;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        ZodiacSignServiceGrpc.ZodiacSignServiceStub zodiacSignServiceStub = ZodiacSignServiceGrpc.newStub(channel);

        boolean isConnected = true;
        while (isConnected) {
            System.out.println("MENU");
            System.out.println("1. Get Zodiac Sign");
            System.out.println("2. QUIT");

            Scanner input = new Scanner(System.in);
            System.out.println("Choose:");
            int option = input.nextInt();

            switch (option) {
                case 1: {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Date (MM/DD/YYY): ");
                    String date = read.next();

                    DateValidator validator = new DateValidatorUsingDateFormat("MM/dd/yyyy");
                    if(!validator.isValid(date)) {
                        System.out.println("Invalid Date Format - MM/DD/YYYY");
                        break;
                    }

                    zodiacSignServiceStub.getZodiacSign(
                            ZodiacSign.Date.newBuilder().setDate(date).build(),

                            new StreamObserver<ZodiacSign.ZodiacSignResponse>() {
                                @Override
                                public void onNext(ZodiacSign.ZodiacSignResponse value) {
                                    System.out.println(value);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    System.out.println("Error: " + t.getMessage() + "\n");
                                }

                                @Override
                                public void onCompleted() {

                                }
                            }
                    );
                    break;
                }
                case 2: {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Unknown command. Please insert a valid command!");
            }

            try {
                TimeUnit.SECONDS.sleep(1);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        channel.shutdown();
    }
}
